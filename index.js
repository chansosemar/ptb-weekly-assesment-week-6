/* 
Javascript
PTB Assesment Week 6
*/

// Tuliskan sebuah fungsi yg mengembalikan nilai sesuai dengan "Expected output"
const firstDay = ["Andi", "Prabowo", "Jokowi", "Roberto"];
const secondDay = ["Sebastian", "Rachel", "Jokowi", "Prabowo"];

const whoIsit = (a, b) => {
  // Ada sebuah konser yg diaadakan 2 hari berturut. coba cari siapakah yg datang di kedua harinya
  // Expected Output ['Jokowi','Prabowo']
};
whoIsit(firstDay, secondDay);

const testCase = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const cariBilanganPrima = (a) => {
  // Temukan bilangan prima dalam array testCase di bawah
  // Expected Output '3 is Prime, so on....'
};

cariBilanganPrima(testCase);
